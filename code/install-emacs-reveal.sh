#!/bin/sh
# Copyright (C) 2018-2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0
#
# Clone emacs-reveal.

mkdir -p /root/.emacs.d/elpa
cd /root/.emacs.d/elpa
git clone https://gitlab.com/oer/emacs-reveal.git
