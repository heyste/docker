#!/bin/sh
# Copyright (C) 2018-2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

cd /tmp
git clone https://code.orgmode.org/bzg/org-mode.git
cd org-mode
git checkout cc020dbe4d9323bc778e50af485dfd902db63444
make lispdir=/usr/share/emacs24/site-lisp/org autoloads
make lispdir=/usr/share/emacs24/site-lisp/org install

emacs --batch --load /tmp/manage-packages.el \
    --eval="(mp-install-pkgs '(org-re-reveal-ref) \"/tmp/archives\")"
