#!/bin/sh
# Copyright (C) 2018-2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0
#
# Clone submodules for emacs-reveal.
# This script expects exactly one argument, the git tag/commit to checkout.

if test -z "$1"
then
    echo "Supply git tag or commit to checkout as argument!"
    exit 1
fi

mkdir -p /root/.emacs.d
cd /root/.emacs.d
git clone https://gitlab.com/oer/emacs-reveal-submodules.git
cd emacs-reveal-submodules
git checkout "$1"
git submodule sync --recursive
git submodule update --init --recursive
