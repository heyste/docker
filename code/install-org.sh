#!/bin/sh
# Copyright (C) 2018-2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

if test -z "$1"
then
    echo "Supply git tag or commit to checkout as argument!"
    exit 1
fi

cd /tmp
git clone https://code.orgmode.org/bzg/org-mode.git
cd org-mode
git checkout $1
make lispdir=/usr/share/emacs24/site-lisp/org autoloads
make lispdir=/usr/share/emacs24/site-lisp/org install
